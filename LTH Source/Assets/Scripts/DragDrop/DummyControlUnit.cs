﻿using UnityEngine;
using System.Collections;
using System.Reflection;

/// <summary>
/// Example of control unit for drag and drop events handle
/// </summary>
public class DummyControlUnit : MonoBehaviour
{
    void OnItemPlace(DragAndDropCell.DropDescriptor desc)
    {
        DummyControlUnit sourceSheet = desc.sourceCell.GetComponentInParent<DummyControlUnit>();
        DummyControlUnit destinationSheet = desc.destinationCell.GetComponentInParent<DummyControlUnit>();
        // If item dropped between different sheets
        if (destinationSheet != sourceSheet)
        {
            //Debug.Log(desc.item.name + " is dropped from " + sourceSheet.name + " to " + destinationSheet.name);
            //Debug.Log(desc.item.tag + " is dropped to " + desc.destinationCell.tag);
            #if UNITY_EDITOR
            var assembly = Assembly.GetAssembly(typeof(UnityEditor.ActiveEditorTracker));
            var type = assembly.GetType("UnityEditorInternal.LogEntries");
            var method = type.GetMethod("Clear");
            method.Invoke(new object(), null);
#endif

            if (desc.item.tag == desc.destinationCell.tag)
            {
                Debug.Log("Correcto");
                FindObjectOfType<GameManager>().puntaje(10);
            }
            else {
                Debug.Log("Incorrecto");
                FindObjectOfType<GameManager>().puntaje(-10 );
            }
            
            GameObject obj = GameObject.Find(desc.item.name);
            Destroy(obj);
        }
    }
}
