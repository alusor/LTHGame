﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerController : MonoBehaviour {

    public float fuerzaSalto = 475f;

    private bool enSuelo = true;
    private bool muere = false;
    public Transform comprobadorSuelo;
    private float comprobadorRadio = 0.05f;
    public LayerMask mascaraSuelo;


    private bool dobleSalto = false;

    private Animator animator;

    private bool corriendo = false;
    public float velocidad = 8f;

    // Use this for initialization
    void Start () {
        //animator = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (corriendo)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(velocidad, GetComponent<Rigidbody2D>().velocity.y);
        }
        //animator.SetFloat("VelX", GetComponent<Rigidbody2D>().velocity.x);
        enSuelo = Physics2D.OverlapCircle(comprobadorSuelo.position, comprobadorRadio, mascaraSuelo);
        //animator.SetBool("IsGrounded", enSuelo);
        if (enSuelo)
        {
            dobleSalto = false;
        }
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        {
            if (corriendo)
            {
                //Salto si puede Saltar
                if (enSuelo || !dobleSalto)
                {
                    //GetComponent<AudioSource>().Play();
                    GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, fuerzaSalto);
                    //rigidbody2D.AddForce(new Vector2(0, fuerzaSalto));
                    if (!dobleSalto && !enSuelo)
                    {
                        dobleSalto = true;
                    }
                }
            }
            else
            {
                corriendo = true;
            }
        }
    }

    IEnumerator OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "enemy")
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
            //animator.SetBool("Muere", true);
            yield return new WaitForSeconds(0.01f);
            gameObject.SetActive(false);

            Debug.Log("Perdiste");
        }

        if (collider.tag == "Finish")
        {
            GetComponent<Rigidbody2D>().isKinematic = true;
            yield return new WaitForSeconds(0.01f);
            gameObject.SetActive(false);
            Debug.Log("Ganaste");
        }
    }
}
