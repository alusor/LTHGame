﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour {
    public int points;
    public int lastLevel;
    public int level;
    private ColectTrash pointer;
    private int puntos;
    public int tiempo = 5;
    private TimerUI time;
    public GameObject modal;
    private Text mensaje;
    public bool sound = true;
    AudioSource Audio;
    private bool final;

	// Use this for initialization
	void Start () {
        Audio = gameObject.AddComponent<AudioSource>();
        Audio.volume = 0.5f;
        pointer = GetComponent<ColectTrash>();
        puntos = 0;
        time = FindObjectOfType<TimerUI>();
        time.time = tiempo;
        time.calcularDelta();
        time.startTime();
        modal.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void puntaje(int numero) {
        puntos += numero;
        Debug.Log(puntos);

        if (sound)
        {
            if (numero > 0) Audio.clip = Resources.Load<AudioClip>("Sounds/Correct");
            if (numero < 0) Audio.clip = Resources.Load<AudioClip>("Sounds/Incorrect");
            if (Audio.clip != null) Audio.Play();
        }
    }
    public void siguienteNivel(int numero) {
        if (final)
        {
            SceneManager.LoadScene(numero);
        }
        else {
            SceneManager.LoadScene(0);
        }
    }
    public void terminoTiempo() {
        Debug.Log("Se termino el tiempo");
       modal.SetActive(true);
        mensaje = GameObject.Find("Mensaje").GetComponent<Text>();
        if (puntos < 50)
        {   
            mensaje.text = "Has perdido.";
            final = false;
        }
        else {
            mensaje.text = "Ganaste " + puntos.ToString() + " puntos.";
            final = true;
            if (PlayerPrefs.HasKey("puntos"))
            {
                PlayerPrefs.SetInt("puntos", PlayerPrefs.GetInt("puntos") + puntos);
            }
            else {
                PlayerPrefs.SetInt("puntos", puntos);
            }
            
        }
        

    }

}
