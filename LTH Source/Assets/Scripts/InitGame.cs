﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitGame : MonoBehaviour {
    public Text puntos;
	// Use this for initialization
	void Start () {
        if (PlayerPrefs.HasKey("puntos"))
            puntos.text = "Puntos: " + PlayerPrefs.GetInt("puntos").ToString();
        else
            puntos.text = "Puntos: 0";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
