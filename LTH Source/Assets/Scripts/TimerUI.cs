﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TimerUI : MonoBehaviour {
	public RectTransform imagen;
	[Range(0,1)]
	public float size;
	public float time;
	public float delta;
	// Use this for initialization
	void Start () {
		
		//StartCoroutine ("Timer");
	}
	
	// Update is called once per frame
	void Update () {
		imagen.anchorMax = new Vector2( size ,imagen.anchorMax.y);

	}
	IEnumerator Timer (){
		while( size>0 ) {
			size -= delta;
			yield return new WaitForSeconds (delta);
		}
        FindObjectOfType<GameManager>().terminoTiempo();
	}
	public void startTime() {
		StartCoroutine ("Timer");
	}
    public void calcularDelta() {
        delta = Time.deltaTime / time;
    }
}
