﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Switch : MonoBehaviour {
	public Sprite on;
	public Sprite off;
    public Sprite son;
    public Sprite soff;
	public GameObject[] focos;
    public GameObject[] swi;
	public bool state;
	// Use this for initialization
	void Start () {
		state = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void changeState() {
		state = !state;
	}
    public void changeSpriteFoco(int id) {
        if (focos[id].GetComponent<SpriteRenderer>().sprite == on)
        {
            focos[id].GetComponent<SpriteRenderer>().sprite = off;
        }
        else {
            focos[id].GetComponent<SpriteRenderer>().sprite = on;
        }
    }
    public void changeSwitch(int id) {
        if (swi[id].GetComponent<Image>().sprite == son)
        {
            swi[id].GetComponent<Image>().sprite = soff;
            FindObjectOfType<GameManager>().puntaje(10);
        }
        else {
            swi[id].GetComponent<Image>().sprite = son;
            FindObjectOfType<GameManager>().puntaje(-10);
        }
    }
}
